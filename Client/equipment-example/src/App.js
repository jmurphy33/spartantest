import React, { Component } from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.css';
import axios from 'axios'
import classnames from 'classnames'
import icons from 'glyphicons'


class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      EquipmentItems: [],
      IsLoaded: false,
      Search: "",
      IsUnitSearch: true
    }
    this.handleChange = this.handleChange.bind(this);
    this.changeToItemNo = this.changeToItemNo.bind(this);
    this.changeToUnitNo = this.changeToUnitNo.bind(this);
  }

  componentWillMount() {
    var config = {
      headers: {'Access-Control-Allow-Origin': '*',
      withCredentials : true,    
    }};

    axios.get('https://localhost:44399/api/equipment',config)
      .then(response => this.setState({ EquipmentItems: response, IsLoaded: true }))
  }

  handleChange(input) {
    this.setState({ Search: input.target.value });
  }

  changeToItemNo() {
    this.setState({ IsUnitSearch: false })
  }

  changeToUnitNo() {
    this.setState({ IsUnitSearch: true })
  }

  render() {
    return (
      <div className="App">

        <Header />
        <div className="container">
          <div className="row content">
            <SearchFilterDiv callback={this.changeToUnitNo} text="Unit No." isChecked={this.state.IsUnitSearch} />
            <SearchFilterDiv callback={this.changeToItemNo} text="Item No." isChecked={!this.state.IsUnitSearch} />
          </div>

          <SearchInputDiv inputValue={this.state.Search} callback={this.handleChange} placeholder="Enter the search item" />

          <DisplayHeader headerText="Results" />
          <EquipmentListDiv IsLoaded={this.state.IsLoaded}
            EquipmentItems={this.state.EquipmentItems}
            Search={this.state.Search}
            IsUnitSearch={this.state.IsUnitSearch} />
        </div>

        <Footer />

      </div>
    );
  }
}

function Header() {
  return (
    <header className="App-header">
      <h1 className="App-title">Spartan PHALANX</h1>
    </header>
  )
}

function Footer() {
  return (
    <div className="App-footer">
      <div className="row">
        <div className="col-lg-2 col-sm-2 col-md-2">
          {icons.arrowW} <br /> <span> Back </span>
        </div>
        <div className="col-lg-10 col-sm-10 col-md-10">
          {icons.menu}  <br /> <span> Options </span>
        </div>

      </div>
    </div>
  )
}

function SearchFilterDiv(props) {
  let classes = classnames("col-lg-6", "col-sm-6", "col-md-6", "content-border",
    { "checked": props.isChecked },
    { "unchecked": !props.isChecked }
  );

  return (
    <div className={classes} onClick={props.callback}>{props.text}</div>
  )
}

function EquipmentListDiv(props) {
  if (props.IsLoaded) {
    var renderedOutput = null;
    var searchArray = props.EquipmentItems.data;

    if (props.Search != "") {
      searchArray = props.EquipmentItems.data.filter((el) => {

        if (props.IsUnitSearch) return el.equipmentExternalId.toString().startsWith(props.Search);

        return el.equipmentTypeExternalId.toString().startsWith(props.Search);
      });

    }

    renderedOutput = searchArray.map(item =>
      <div className="row content-body">
        <div className="col-lg-1 col-sm-1 col-md-1">
          <p className="sidebar"></p>
        </div>
        <div className="col-lg-10 col-sm-10 col-md-10 left">
          {item.equipmentExternalId} <br />
          {item.equipmentTypeExternalId} <br />
          {item.equipmentTypeDescription}
        </div>
        <div className="col-lg-1 col-sm-1 col-md-1">
          <i> > </i>
        </div>
      </div>
    )
    return (<div>{renderedOutput} </div>)

  }
  else {
    return (
      <div>
        Loading...
      </div>
    );

  }

}

function SearchInputDiv(props) {
  return (
    <div className="row content-border">
      <div className="col-lg-11 col-sm-11 col-md-11">
        <input type="text"
          value={props.inputValue}
          className="search-input"
          onChange={props.callback}
          placeholder={props.placeholder} />
      </div>
      <div className="col-lg-1 col-sm-1 col-md-1">
        {icons.magnifyingGlass}
      </div>
    </div>
  )
}

function DisplayHeader(props) {
  return (
    <div className="row content-header">
      <div className="col-lg-11 col-sm-11 col-md-11">
        <span className="left">{props.headerText}</span>
      </div>
      <div className="col-lg-1 col-sm-1 col-md-1">
        {icons.memo}
      </div>
    </div>
  )
}

export default App;
