
Requirements:

- node package manager: https://nodejs.org/en/download/

- Visual Studio 2017 to run/view server: https://visualstudio.microsoft.com/vs/community/

- Text Editor (VS Code) to view client: https://code.visualstudio.com/

- IIS Express: Installed with Visual Studio / https://www.microsoft.com/en-us/download/details.aspx?id=48264

- Browser of your choice



Startup instructions:

- Start server first from : ~/spartantest/Server/ExampleTest.sln

- To start the client open a command prompt in: ~/spartantest/Client/equipment-example

   
    - type (first time use only): npm install
        this will restore any missing packages
    
    - type: npm start
        this will start the server on localhost:3000
    


Known Issues:

- CORS conflicts occur on FireFox. Should be resolved for other browsers.

- Extensions for each browser can be used to over come this:

    - Allow-Control-Allow-Origin for Chrome/FireFox.