﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ExampleTest.Models.DTOs;
using ExampleTest.Models.JsonModels;
using Newtonsoft.Json;

namespace ExampleTest
{
    /// <summary>
    /// Static Mock class to emulate a repository for <see cref="EquipmentDto"/> 
    /// </summary>
    internal static class RepositoryMock
    {
        /// <summary>
        ///  <see cref="EquipmentDto"/> repository to access data
        /// </summary>
        internal static List<EquipmentDto> EquipmentRepository { get; private set; }

        /// <summary>
        /// Initialise funtionality to read the JSON data and setup repository
        /// </summary>
        internal static void Init()
        {
            var serializer = new JsonSerializer();
            EquipmentRepository = new List<EquipmentDto>();

            using (var file = System.IO.File.OpenText(@"Resources\EquipmentData.json"))
            using (var jsonTextReader = new JsonTextReader(file))
            {
                var equipment = (EquipmentJson)serializer.Deserialize(jsonTextReader, typeof(EquipmentJson));

                var result = equipment.SerialisedEquipment.Select(c => new EquipmentDto {
                    EquipmentExternalId = c.ExternalId,
                    EquipmentTypeExternalId = equipment.EquipmentType.First(y => y.Id == c.EquipmentTypeId).ExternalId,
                    EquipmentTypeDescription = equipment.EquipmentType.First(y => y.Id == c.EquipmentTypeId).Description
                }).ToList();

                EquipmentRepository = result;
            }
        }    
    }
}
