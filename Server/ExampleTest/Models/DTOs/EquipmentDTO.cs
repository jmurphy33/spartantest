﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExampleTest.Models.DTOs
{
    /// <summary>
    /// DTO for <see cref="SerialisedEquipment"/> and <see cref="EquipmentType"/>
    /// </summary>
    public class EquipmentDto
    {
        /// <summary>
        /// External ID Belong to <see cref="SerialisedEquipment"/> 
        /// </summary>
        public int EquipmentExternalId { get; set; }

        /// <summary>
        /// External ID Belong to <see cref="EquipmentType"/>
        /// </summary>
        public int EquipmentTypeExternalId { get; set; }

        /// <summary>
        /// Description Belong to <see cref="EquipmentType"/>
        /// </summary>
        public string EquipmentTypeDescription{ get; set; }
    }
}
