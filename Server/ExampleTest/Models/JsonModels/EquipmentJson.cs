﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ExampleTest.Models;

namespace ExampleTest.Models.JsonModels
{
    /// <summary>
    /// JSON Model required to serialise and deserialise EquipmentJson.cs
    /// </summary>
    public class EquipmentJson
    {
        /// <summary>
        /// <see cref="Models.SerialisedEquipment"/> array from JSON
        /// </summary>
        [JsonProperty(PropertyName = "SerialisedEquipment")]
        public List<SerialisedEquipment> SerialisedEquipment { get; set; }

        /// <summary>
        /// <see cref="Models.EquipmentType"/> array from JSON
        /// </summary>
        [JsonProperty(PropertyName = "EquipmentType")]
        public List<EquipmentType> EquipmentType { get; set; }
    }
}
