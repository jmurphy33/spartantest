﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExampleTest.Models
{
    /// <summary>
    ///  <see cref="EquipmentType"/> Model
    /// </summary>
    public class EquipmentType
    {
        /// <summary>
        /// Id for <see cref="EquipmentType"/>
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// External ID for <see cref="EquipmentType"/>
        /// </summary>
        public int ExternalId { get; set; }

        /// <summary>
        /// Description for <see cref="EquipmentType"/>
        /// </summary>
        public string Description { get; set; }
    }

}
