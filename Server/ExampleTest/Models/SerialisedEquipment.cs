﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExampleTest.Models
{
    /// <summary>
    /// Model for <see cref="SerialisedEquipment"/>
    /// </summary>
    public class SerialisedEquipment
    {
        /// <summary>
        /// ID for <see cref="SerialisedEquipment"/>
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// ExternalId for <see cref="SerialisedEquipment"/>
        /// </summary>
        public int ExternalId { get; set; }

        /// <summary>
        /// EquipemtType ID to reference <see cref="EquipmentType"/> for <see cref="SerialisedEquipment"/>
        /// </summary>
        public Guid EquipmentTypeId { get; set; }

        /// <summary>
        /// MeterReading for <see cref="SerialisedEquipment"/>
        /// </summary>
        public int MeterReading { get; set; }
    }
}
