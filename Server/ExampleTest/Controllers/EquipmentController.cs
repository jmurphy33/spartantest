﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ExampleTest.Models;
using ExampleTest.Models.DTOs;
using Microsoft.ApplicationInsights.AspNetCore;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace ExampleTest.Controllers
{
    /// <summary>
    /// Controller to access the <see cref="EquipmentDto"/> repository
    /// </summary>
    [Route("api/[controller]")]
    [EnableCors("AllowSpecificOrigin")]
    public class EquipmentController : Controller
    {

        // GET api/values
        /// <summary>
        /// Get method for returning all <see cref="EquipmentDto"/> 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Produces("application/json")]
        [EnableCors("CorsPolicy")]
        public JsonResult Get()
        {
            var equipment = RepositoryMock.EquipmentRepository;
            return Json(equipment);
        }
    }
}
